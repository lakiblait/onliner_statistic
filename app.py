from flask import Flask, render_template, request, redirect, url_for, json
import json
from sqlalchemy import Column, Integer, String, Date, Float, ForeignKey, distinct
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from operator import itemgetter

engine = create_engine("sqlite:///Products.db", echo=True)
Base = declarative_base()


class Products(Base):
    __tablename__ = 'Products'
    id = Column(Integer, primary_key=True)
    id_product = Column(Integer)
    key = Column(String)
    name = Column(String)
    html_url = Column(String)
    url = Column(String)
    prices_url = Column(String)

    def __init__(self, id_product, key, name, html_url, url, prices_url):
        self.id_product = id_product
        self.key = key
        self.name = name
        self.html_url = html_url
        self.url = url
        self.prices_url = prices_url


class Shops(Base):
    __tablename__ = "Shops"
    id = Column(Integer, primary_key=True)
    id_shop = Column(Integer)
    name = Column(String)
    html_url = Column(String)

    def __init__(self, id_shop, name, html_url):
        self.id_shop = id_shop
        self.name = name
        self.html_url = html_url


class Prices(Base):
    __tablename__ = "Prices"
    id = Column(Integer, primary_key=True)
    time = Column(Date)
    price = Column(Float)
    product_id = Column(Integer, ForeignKey(Products.id))
    shop_id = Column(Integer, ForeignKey(Shops.id))

    def __init__(self, time, price, product_id, shop_id):
        self.time = time
        self.price = price
        self.product_id = product_id
        self.shop_id = shop_id


def group_data(data):
    buff_shop = data[0][2]
    data_group = list()
    sub_list = list()
    for row in enumerate(data):
        if buff_shop != row[1][2]:
            buff_shop = row[1][2]
            data_group.append(sub_list)
            sub_list = list()
        sub_list.append(row[1])
        if row[0] == len(data) - 1:
            data_group.append(sub_list)
    return data_group


def get_series(data_group, shops, date):
    series = list()
    for group in data_group:
        line = dict()
        line["seriesType"] = "spline"
        line["name"] = shops[group[0][2]]
        points = list()
        for date_i in date:
            buff = 0
            for j in group:
                if date_i[0] == j[0]:
                    points.append({"x": str(j[0]), "value": j[1]})
                    buff += 1
            if buff == 0:
                points.append({"x": str(date_i[0]), "value": 0})
        line["data"] = points
        series.append(line)
    return series


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "GET":
        return render_template('index.html')
    elif request.method == "POST":
        if 'https' not in request.form["Search"]:
            return redirect('/')
        else:
            url_db = request.form["Search"]
            pr_id_name = session.query(Products.id, Products.name).filter(Products.html_url == url_db).first()
            data = session.query(Prices.time, Prices.price, Prices.shop_id).filter(
                Prices.product_id == pr_id_name[0]).all()
            data = sorted(data, key=itemgetter(2, 0))
            date = session.query(distinct(Prices.time)).filter(Prices.product_id == pr_id_name[0]).all()
            shops = dict(session.query(Shops.id, Shops.name).all())
            http_shops = dict(session.query(Shops.name, Shops.html_url).all())
            session.commit()
            data_group = group_data(data)
            series = get_series(data_group, shops, date)
            chart = {
                "chart": {
                    "type": "line",
                    "series": series,
                    "title": pr_id_name[1],
                    "animation": True,
                    "legend": True,
                    "tooltip": {"background": {"fill": "#343a40"}},
                    "container": "container"
                }
            }
            return render_template('graph.html', chartData=json.dumps(chart), name=pr_id_name[1], series=series,
                                   first_series=series[0]["data"], http_shops=http_shops)


if __name__ == "__main__":
    app.run(debug=True)
