import json
import requests
from time import sleep
from sqlalchemy import Column, Integer, String, Date, Float, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from datetime import datetime


'''
Категории товаров, которые будем парсить разово (пока в json далее в DB)
"https://catalog.onliner.by/sdapi/catalog.api/search/mobile" - Телефоны
'''

engine = create_engine("sqlite:///Products.db", echo=True)
Base = declarative_base()


class Products(Base):
    __tablename__ = 'Products'
    id = Column(Integer, primary_key=True)
    id_product = Column(Integer)
    key = Column(String)
    name = Column(String)
    html_url = Column(String)
    url = Column(String)
    prices_url = Column(String)

    def __init__(self, id_product, key, name, html_url, url, prices_url):
        self.id_product = id_product
        self.key = key
        self.name = name
        self.html_url = html_url
        self.url = url
        self.prices_url = prices_url


class Shops(Base):
    __tablename__ = "Shops"
    id = Column(Integer, primary_key=True)
    id_shop = Column(Integer)
    name = Column(String)
    html_url = Column(String)

    def __init__(self, id_shop, name, html_url):
        self.id_shop = id_shop
        self.name = name
        self.html_url = html_url


class Prices(Base):
    __tablename__ = "Prices"
    id = Column(Integer, primary_key=True)
    time = Column(Date)
    price = Column(Float)
    product_id = Column(Integer, ForeignKey(Products.id))
    shop_id = Column(Integer, ForeignKey(Shops.id))

    def __init__(self, time, price, product_id, shop_id):
        self.time = time
        self.price = price
        self.product_id = product_id
        self.shop_id = shop_id


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()


def get_models_parameters(url: str):
    response = requests.get(url, params={})
    pages = response.json()['page']['last']
    all_models = dict()
    for num_page in range(1, pages + 1):  #
        response = requests.get(url, params={"page": num_page})
        data = response.json()["products"]
        for n_pr in data:
            if n_pr['prices'] is None or n_pr.get("prices") is None:
                all_models[n_pr['id']] = {'name': n_pr['full_name'], 'key': n_pr['key'], 'html_url': n_pr['html_url'],
                                          "url": n_pr["url"],
                                          'prices_url': f"https://catalog.onliner.by/sdapi/shop.api/products/{n_pr['key']}/positions"}
            else:
                all_models[n_pr['id']] = {'name': n_pr['full_name'], 'key': n_pr['key'], 'html_url': n_pr['html_url'],
                                          "url": n_pr["url"],
                                          'prices_url': n_pr['prices']['url']}
        print(num_page)
        sleep(1)
    return all_models


def make_json(name, data):
    with open(name + '.json', 'w') as f:
        json.dump(data, f)


def get_names_shop(filename):
    with open('Json/' + filename + '.json', 'r') as f:
        data = json.load(f)
        all_shops = dict()
        a = 1
        for key in data:
            print(key, a)
            response = requests.get(data[key]["prices_url"], params={})
            try:
                shops = response.json()["shops"]
            except:
                print('Успели убрать цену')
            for shop_id in shops:
                if all_shops.get(shop_id) is not None:
                    continue
                all_shops[shop_id] = {"name": shops[shop_id]['title'], "html_url": shops[shop_id]["html_url"]}
            sleep(1)
            a += 1
        with open("Json/" + filename + '_shops.json', 'w') as f_w:
            json.dump(all_shops, f_w)


def site_survey():
    begin_time = datetime.now()
    shops = dict(session.query(Shops.id_shop, Shops.id).all())
    urls_price = session.query(Products.id, Products.prices_url, Products.name).all()
    to_db = list()
    a = 1
    for i in urls_price:
        id_prod = i[0]
        pr_url = i[1]
        name_prod = i[2]
        print(a, name_prod)
        response = requests.get(pr_url, params={})
        data = response.json()
        if 'positions' not in data:
            for k in shops:
                pr = Prices(time=datetime.date(datetime.now()),
                            price=float(0),
                            product_id=id_prod,
                            shop_id=shops[k])
                to_db.append(pr)
        else:
            data_product = data["positions"]["primary"]
            for j in data_product:
                try:
                    pr = Prices(time=datetime.date(datetime.now()),
                                price=float(j["position_price"]["amount"]),
                                product_id=id_prod,
                                shop_id=shops[j["shop_id"]])
                    to_db.append(pr)
                except KeyError:
                    session.add(Shops(id_shop=j["shop_id"],
                                      name=data["shops"][str(j["shop_id"])]["title"],
                                      html_url=data["shops"][str(j["shop_id"])]["html_url"]))
                    session.commit()
                    shops = dict(session.query(Shops.id_shop, Shops.id).all())
                    pr = Prices(time=datetime.date(datetime.now()),
                                price=float(j["position_price"]["amount"]),
                                product_id=id_prod,
                                shop_id=shops[j["shop_id"]])
                    to_db.append(pr)
        sleep(1)
        if a % 1000 == 0:
            session.add_all(to_db)
            to_db = list()
        a += 1
    session.add_all(to_db)
    session.commit()
    end_time = datetime.now()
    print(end_time - begin_time)


def main():
    site_survey()


if __name__ == "__main__":
    main()
# with open('duct = int(k)
#         key = data[k]["key"]
#         name = data[k]["name"]
#         html_url = data[k]["html_url"]
#         url = data[k]["url"]
#         prices_url = data[k]["prices_url"]
#         pr = Products(id_product=id_product, key=key, name=name, html_url=html_url, url=url, prices_url=prices_url)
#         toDB.append(pr)
#     session.add_all(toDB)
#     session.commit()


# with open('Json/mobile_shops.json', 'r') as f:
#     data = json.load(f)
#     toDB = list()
#     for k in data:
#         id_shop = int(k)
#         name = data[k]["name"]
#         html_url = data[k]["html_url"]
#         pr = Shops(id_shop=id_shop, name=name, html_url=html_url)
#         toDB.append(pr)
#     session.add_all(toDB)
#     session.commit()


# ---------------------------------------------------------------------------------------------------------------------
# response = requests.get("https://catalog.onliner.by/sdapi/shop.api/products/k203b/positions", params={})
# pages = response.json()
# print(pages)
# with open('Json/' + "mobile" + '.json', 'r') as f:
#     data = json.load(f)
#     a = 1
#     for i in data:
#         if a == 752:
#             print(i)
#         a += 1
# get_names_shop("mobile")

# links = ["https://catalog.onliner.by/sdapi/catalog.api/search/mobile",
#          "https://catalog.onliner.by/sdapi/catalog.api/search/tv",
#          "https://catalog.onliner.by/sdapi/catalog.api/search/notebook"]
#

# inf = get_models_parameters("https://catalog.onliner.by/sdapi/catalog.api/search/mobile")
# make_json("Json/" + "https://catalog.onliner.by/sdapi/catalog.api/search/mobile".split("/")[-1], inf)
# with open('Json/' + "mobile" + '.json', 'r') as f:
#     data = json.load(f)
#     all_shops = dict()
#     all_shops['965']=1
#     response = requests.get(data['1755366']["prices_url"], params={})
#     shops = response.json()["shops"]
#     for shop_id in shops:
#         if all_shops.get(shop_id) is not None:
#             continue
#         all_shops[shop_id] = {"name": shops[shop_id]['title'], "html_url": shops[shop_id]["html_url"]}
# print(all_shops)

# url = "https://catalog.onliner.by/mobile/huawei/p40litep"
# pr_id = session.query(Products.id,Products.name).filter(Products.html_url == url).first()
# data = session.query(Prices.time, Prices.price, Prices.shop_id).filter(
#     Prices.product_id == pr_id[0]).all()  # .in_([pr_id[0],1])
# data = sorted(data, key=itemgetter(2, 0))
# date = session.query(distinct(Prices.time)).filter(Prices.product_id == pr_id[0]).all()
#
# shops = dict(session.query(Shops.id, Shops.name).all())
# session.commit()
# data_group = list()
# sub_list = list()
# buff_shop = data[0][2]
#
# for row in enumerate(data):
#     if buff_shop != row[1][2]:
#         buff_shop = row[1][2]
#         data_group.append(sub_list)
#         sub_list = list()
#     sub_list.append(row[1])
#     if row[0] == len(data) - 1:
#         data_group.append(sub_list)
# series = list()
# for group in data_group:
#     line = dict()
#     line["seriesType"] = "spline"
#     line["name"] = shops[group[0][2]]
#     points = list()
#     for date_i in date:
#         buff = 0
#         for j in group:
#             if date_i[0] == j[0]:
#                 points.append({"x": str(j[0]), "value": j[1]})
#                 buff += 1
#         if buff == 0:
#             points.append({"x": str(date_i[0]), "value": 0})
#     line["data"] = points
#     series.append(line)
# pprint(series)
#
# chartData = json.dumps(series)
